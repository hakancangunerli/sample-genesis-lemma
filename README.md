## Badges
People seem to like these, so.

[![Python](https://img.shields.io/badge/Python-3776AB?style=for-the-badge&logo=python&logoColor=white)]()
[![Jupyter](https://img.shields.io/badge/Jupyter-F37626.svg?&style=for-the-badge&logo=Jupyter&logoColor=white)]()
# Genesis-lemma

This is a sample project for tokenization and spaCy tutorial . 


## Authors

- [@hakancangunerli](https://www.github.com/hakancangunerli)

  
## Installation 

Clone first. 

Select the file you want to run, and run the jupyter notebook. done.

    
## Acknowledgements
Note: this is not 100% my code, since I didn't write spaCy and stuff.
